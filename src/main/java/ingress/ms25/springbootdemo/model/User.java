package ingress.ms25.springbootdemo.model;

import jakarta.persistence.*;


@Entity
@Table(name = "Users", schema = "Config")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public String id;

    public String name;
}
