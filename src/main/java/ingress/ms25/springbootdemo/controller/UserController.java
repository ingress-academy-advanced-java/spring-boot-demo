package ingress.ms25.springbootdemo.controller;

import ingress.ms25.springbootdemo.model.User;
import ingress.ms25.springbootdemo.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable String id) {
        return userService.findById(id);
    }

    @PostMapping
    public void createUser(@RequestBody User user) {
         userService.save(user);
    }
}
