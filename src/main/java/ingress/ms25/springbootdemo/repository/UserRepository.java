package ingress.ms25.springbootdemo.repository;

import ingress.ms25.springbootdemo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
